SHELL=/bin/bash
PATH := .venv/bin:$(PATH)
export TEST?=./tests
export LAMBDA?=sonarqube
export IMG_NAME=img-sonarqube
export REPO=lambda-${LAMBDA}
export ENV?=dev
export ECR_URL=${AWS_ACCOUNT_ID}.dkr.ecr.${AWS_REGION}.amazonaws.com
export IMG_TAG=latest
export VERSION=latest

install:
	@( \
		if [ ! -d .venv ]; then python3 -m venv --copies .venv; fi; \
		source .venv/bin/activate; \
		pip install -qU pip; \
		pip install -r requirements.txt; \
	)

setup:
	@if [ ! -f .env ] ; then cp .env.mock .env ; fi;
	@make install;

autoflake:
	@autoflake . --check --recursive --remove-all-unused-imports --remove-unused-variables --exclude .venv;

black:
	@black . --check --exclude '.venv|build|target|dist|.cache|node_modules';

isort:
	@isort . --check-only;

lint: black isort autoflake

lint-fix:
	@black . --exclude '.venv|build|target|dist';
	@isort .;
	@autoflake . --in-place --recursive --exclude .venv --remove-all-unused-imports --remove-unused-variables;

docs:
	@if [ ! -f ./docs/make.bat ]; then (cd docs && sphinx-quickstart); fi;
	@(cd docs && make html);
	@if command -v open; then open ./docs/*build/html/index.html; fi;

tests:
	@python -B -m pytest -l --color=yes \
		--cov=src \
		--cov-config=./tests/.coveragerc \
		--cov-report term \
		--cov-report html:coverage \
		--junit-xml=junit.xml \
		--rootdir=. $${TEST};

build-package:
	@echo "Building package";
	@if [ ! -d terraform/global/build ]; then mkdir ./terraform/global/build; fi;
	@pip install -r requirements.txt -t ./requirements;
	@cp -r ./requirements ./temporary_directory/;
	@cp -r  ./src ./temporary_directory/;
	@cp ./requirements.txt ./index.py ./temporary_directory/;
	@(cd ./temporary_directory && zip -r talos-voice-generator-endpoint.zip ./*);
	@if [ -d ./terraform/global/build/requirements ]; then rm -r ./terraform/global/build/requirements; fi;
	@mv ./requirements ./temporary_directory/talos-voice-generator-endpoint.zip ./terraform/global/build/;
	@rm -r ./temporary_directory;

validate-tf-workspaces:
	@( \
		cd terraform/workspaces;  \
		terraform init; \
		printf "\033[0;34mValidating workspaces...\034\n"; \
		terraform validate; \
	)


validate-tf:
	@( \
		cd terraform/global;  \
		terraform init; \
		printf "\033[0;34mValidating terraform...\034\n"; \
		terraform validate; \
	)

validate-plan: 
	@( \
		cd terraform;  \
		terraform init; \
		printf "\033[0;34mValidating terraform...\034\n"; \
		terraform validate; \
		printf "\033[0;34mPlanning terraform...\034\n"; \
		terraform plan; \
	)

validate-apply:
	@( \
		cd terraform;  \
		terraform init; \
		printf "\033[0;34mValidating terraform...\034\n"; \
		terraform validate; \
		printf "\033[0;34mPlanning terraform...\034\n"; \
		terraform plan; \
		printf "\033[0;34mApplying terraform...\034\n"; \
		terraform apply -auto-approve; \
	)

validate-terraform: validate-tf-workspaces validate-tf
	@printf "\033[0;34mGenerating execution plan updates...\034\n";
	@(cd terraform/ && terraform plan);

invoke-local:
	@cat event.local.json | base64 > event.local.base64
	@aws lambda invoke --function-name $${LAMBDA} --payload file://event.json output.json
	@if [ -f response.json ]; then python -B -m json.tool response.json; fi
	@rm -f event.local.base64
    
build:
	@docker build -t ${LAMBDA} .

run:
	@(\
	docker run -d -p 9001:9000 \
	  -e sonar.jdbc.username=sonar \
	  -e sonar.jdbc.password=sonar \
	  -v sonarqube_conf:/opt/sonarqube/conf \
	  -v sonarqube_extensions:/opt/sonarqube/extensions \
	  -v sonarqube_logs:/opt/sonarqube/logs \
	  -v sonarqube_data:/opt/sonarqube/data \
	  ${LAMBDA} \
	)

.PHONY: tests docs

### Database Module

start-db:
	@docker-compose -f docker-compose.yaml up -d; 

restart-db:
	@rm -rf postgres-data; \
	docker-compose -f docker-compose.yaml down; \
	docker-compose -f docker-compose.yaml up -d;