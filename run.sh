#!/usr/bin/env bash

set -euo pipefail
declare -a sq_opts=()

# if nothing is passed, assume we want to run SonarQube server
if [ "$#" == 0 ]; then
    set -- bin/sonar.sh
fi

# if the first argument looks like a flag, assume we want to run SonarQube server with flags
if [ "${1:0:1}" = '-' ]; then
    set -- bin/sonar.sh "$@"
fi

if [[ "$1" = 'bin/sonar.sh' ]]; then
    chown -R "$(id -u):$(id -g)" "${SQ_DATA_DIR}" "${SQ_EXTENSIONS_DIR}" "${SQ_LOGS_DIR}" "${SQ_TEMP_DIR}" 2>/dev/null || :
    chmod -R 700 "${SQ_DATA_DIR}" "${SQ_EXTENSIONS_DIR}" "${SQ_LOGS_DIR}" "${SQ_TEMP_DIR}" 2>/dev/null || :

    # Allow the container to be started with `--user`
    if [[ "$(id -u)" = '0' ]]; then
        chown -R sonarqube:sonarqube /opt/sonarqube
        chmod -R 755 /opt/sonarqube/temp
        su sonarqube -s /bin/bash -c "$0 $@"
    fi
    


    # Deprecated way to pass settings to SonarQube that will be removed in future versions.
    # Please use environment variables (https://docs.sonarqube.org/latest/setup/environment-variables/)
    # instead to customize SonarQube.
    while IFS='=' read -r envvar_key envvar_value
    do
        if [[ "$envvar_key" =~ sonar.* ]] || [[ "$envvar_key" =~ ldap.* ]]; then
            sq_opts+=("-D${envvar_key}=${envvar_value}")
        fi
    done < <(env)

    if [ ${#sq_opts[@]} -ne 0 ]; then
        set -- "$@" "${sq_opts[@]}"
    fi
fi

exec "$@"

